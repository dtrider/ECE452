package ca.uwaterloo.dtqzzy.ece452.models.properties

import com.google.gson.JsonObject

interface JsonSerializable {
    fun toJson() : JsonObject
}
