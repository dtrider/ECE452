package ca.uwaterloo.dtqzzy.ece452.models.db.converters

import android.arch.persistence.room.TypeConverter
import ca.uwaterloo.dtqzzy.ece452.models.RestrictionType
import org.threeten.bp.Duration
import org.threeten.bp.OffsetDateTime
import org.threeten.bp.format.DateTimeFormatter

object RestrictionTypeConverter {
    @TypeConverter
    @JvmStatic
    fun toRestrictionType(value: String?): RestrictionType {
        return RestrictionType.fromString(value)
    }

    @TypeConverter
    @JvmStatic
    fun fromRestrictionType(restrictionType: RestrictionType?): String {
        return restrictionType?.toString() ?: RestrictionType.UNKNOWN.toString()
    }
}