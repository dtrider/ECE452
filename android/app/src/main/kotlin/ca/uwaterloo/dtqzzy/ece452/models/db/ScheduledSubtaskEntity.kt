package ca.uwaterloo.dtqzzy.ece452.models.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import ca.uwaterloo.dtqzzy.ece452.models.ScheduledSubtask
import ca.uwaterloo.dtqzzy.ece452.models.Subtask
import ca.uwaterloo.dtqzzy.ece452.models.properties.JsonDeserializable
import com.google.gson.JsonObject
import org.threeten.bp.OffsetDateTime

@Entity(tableName = "scheduled_subtask")
data class ScheduledSubtaskEntity(
        @PrimaryKey(autoGenerate = true) var id: Long?,
        @ColumnInfo(name = "subtask_json") var subtaskJson: String,
        @ColumnInfo(name = "start_datetime") override var startDateTime: OffsetDateTime,
        @ColumnInfo(name = "end_datetime") override var endDateTime: OffsetDateTime
) : ScheduledSubtask() {
    constructor(id: Long?, subtask: Subtask, startDateTime: OffsetDateTime, endDateTime: OffsetDateTime)
            : this(id, subtask.toJson().toString(), startDateTime, endDateTime)

    override var subtask: Subtask
        get() = SubtaskEntity.fromJson(subtaskJson)
        set(value) {
            subtaskJson = value.toJson().toString()
        }

    override fun toJson(): JsonObject {
        val jsonObject = super.toJson()
        jsonObject.addProperty("id", id)

        return jsonObject
    }

    companion object : JsonDeserializable<ScheduledSubtaskEntity> {
        @JvmStatic
        override fun fromJson(jsonObject: JsonObject): ScheduledSubtaskEntity = ScheduledSubtaskEntity(
                jsonObject.get("id")?.asLong,
                SubtaskEntity.fromJson(jsonObject.get("subtask").asJsonObject),
                OffsetDateTime.parse(jsonObject.get("startDateTime")?.asString),
                OffsetDateTime.parse(jsonObject.get("endDateTime")?.asString))

        @JvmStatic
        override fun fromJson(jsonString: String): ScheduledSubtaskEntity = super.fromJson(jsonString)
    }
}
