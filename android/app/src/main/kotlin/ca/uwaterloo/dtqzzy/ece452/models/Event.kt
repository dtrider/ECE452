package ca.uwaterloo.dtqzzy.ece452.models

import ca.uwaterloo.dtqzzy.ece452.models.properties.DateTimeRange
import ca.uwaterloo.dtqzzy.ece452.models.properties.JsonSerializable
import com.google.gson.JsonObject
import org.threeten.bp.Duration
import org.threeten.bp.OffsetDateTime
import org.threeten.bp.format.DateTimeFormatter

/**
 * An event from an user's calendar. Contains a nullable reference to a restriction.
 */
abstract class Event : DateTimeRange, JsonSerializable {
    abstract var name: String
    abstract var source: String?
    abstract override var startDateTime: OffsetDateTime
    abstract override var endDateTime: OffsetDateTime

    override fun toJson(): JsonObject {
        val jsonObject = JsonObject()
        jsonObject.addProperty("name", name)
        jsonObject.addProperty("source", source)
        jsonObject.addProperty("startDateTime", startDateTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME))
        jsonObject.addProperty("endDateTime", endDateTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME))

        return jsonObject
    }
}
