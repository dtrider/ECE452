package ca.uwaterloo.dtqzzy.ece452.models

import ca.uwaterloo.dtqzzy.ece452.models.properties.JsonSerializable
import com.google.gson.JsonObject

/**
 * A category under that a task is under.
 */
abstract class Category : JsonSerializable {
    abstract var name: String

    override fun toJson(): JsonObject {
        val jsonObject = JsonObject()
        jsonObject.addProperty("name", name)

        return jsonObject
    }
}
