package ca.uwaterloo.dtqzzy.ece452.models

import ca.uwaterloo.dtqzzy.ece452.models.properties.JsonSerializable
import com.google.gson.JsonObject
import org.threeten.bp.Duration
import org.threeten.bp.OffsetDateTime
import org.threeten.bp.format.DateTimeFormatter

/**
 * A subtask created by the user.
 */
abstract class Subtask : JsonSerializable {
    abstract var name: String
    abstract var duration: Duration
    abstract var deadline: OffsetDateTime

    override fun toJson(): JsonObject {
        val jsonObject = JsonObject()
        jsonObject.addProperty("name", name)
        jsonObject.addProperty("duration", duration.toMillis())
        jsonObject.addProperty("deadline", deadline.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME))

        return jsonObject
    }
}
