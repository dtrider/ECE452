package ca.uwaterloo.dtqzzy.ece452.models.properties

import com.google.gson.JsonObject
import com.google.gson.JsonParser

interface JsonDeserializable<out T> {
    fun fromJson(jsonObject: JsonObject): T
    fun fromJson(jsonString: String): T = fromJson(JsonParser().parse(jsonString).asJsonObject)
}
