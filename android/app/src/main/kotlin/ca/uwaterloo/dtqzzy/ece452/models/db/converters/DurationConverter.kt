package ca.uwaterloo.dtqzzy.ece452.models.db.converters

import android.arch.persistence.room.TypeConverter
import org.threeten.bp.Duration
import org.threeten.bp.OffsetDateTime
import org.threeten.bp.format.DateTimeFormatter

object DurationConverter {
    @TypeConverter
    @JvmStatic
    fun toDuration(value: Long?): Duration? {
        return value?.let {
            return Duration.ofSeconds(value)
        }
    }

    @TypeConverter
    @JvmStatic
    fun fromDuration(duration: Duration?): Long? {
        return duration?.seconds
    }
}