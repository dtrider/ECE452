package ca.uwaterloo.dtqzzy.ece452.models.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import ca.uwaterloo.dtqzzy.ece452.models.Subtask
import ca.uwaterloo.dtqzzy.ece452.models.properties.JsonDeserializable
import com.google.gson.JsonObject
import com.google.gson.JsonParseException
import org.threeten.bp.Duration
import org.threeten.bp.OffsetDateTime

@Entity(tableName = "subtask")
data class SubtaskEntity(
        @PrimaryKey(autoGenerate = true) var id: Long?,
        @ColumnInfo(name = "subtask_name") override var name: String,
        @ColumnInfo(name = "duration") override var duration: Duration,
        @ColumnInfo(name = "deadline") override var deadline: OffsetDateTime
) : Subtask() {
    override fun toJson(): JsonObject {
        val jsonObject = super.toJson()
        jsonObject.addProperty("id", id)

        return jsonObject
    }

    companion object : JsonDeserializable<SubtaskEntity> {
        @JvmStatic
        override fun fromJson(jsonObject: JsonObject): SubtaskEntity = SubtaskEntity(
                jsonObject.get("id")?.asLong,
                jsonObject.get("name")?.asString
                        ?: throw JsonParseException("Json object has no name field or a null name"),
                Duration.ofMillis(jsonObject.get("duration").asLong),
                OffsetDateTime.parse(jsonObject.get("deadline")?.asString))

        @JvmStatic
        override fun fromJson(jsonString: String): SubtaskEntity = super.fromJson(jsonString)
    }
}
