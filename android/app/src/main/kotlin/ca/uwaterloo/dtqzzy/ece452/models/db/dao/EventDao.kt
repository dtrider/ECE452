package ca.uwaterloo.dtqzzy.ece452.models.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import ca.uwaterloo.dtqzzy.ece452.models.db.EventEntity

@Dao
abstract class EventDao : WorkDao<EventEntity> {
    @Query("SELECT * FROM event ORDER BY datetime(start_datetime)")
    abstract fun getEvents(): List<EventEntity>

    @Query("DELETE FROM event")
    abstract fun deleteAll()
}
