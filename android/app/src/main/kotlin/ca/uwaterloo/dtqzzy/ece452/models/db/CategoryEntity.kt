package ca.uwaterloo.dtqzzy.ece452.models.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import ca.uwaterloo.dtqzzy.ece452.models.Category
import ca.uwaterloo.dtqzzy.ece452.models.properties.JsonDeserializable
import com.google.gson.JsonObject
import com.google.gson.JsonParseException

@Entity(tableName = "category")
data class CategoryEntity(
        @PrimaryKey(autoGenerate = true) var id: Long?,
        @ColumnInfo(name = "name") override var name: String
) : Category() {
    override fun toJson(): JsonObject {
        val jsonObject = super.toJson()
        jsonObject.addProperty("id", id)

        return jsonObject
    }

    companion object : JsonDeserializable<CategoryEntity> {
        @JvmStatic
        override fun fromJson(jsonObject: JsonObject): CategoryEntity = CategoryEntity(
                jsonObject.get("id")?.asLong,
                jsonObject.get("name")?.asString
                        ?: throw JsonParseException("Json object has no name field"))

        @JvmStatic
        override fun fromJson(jsonString: String): CategoryEntity = super.fromJson(jsonString)
    }
}
