package ca.uwaterloo.dtqzzy.ece452.models.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import ca.uwaterloo.dtqzzy.ece452.models.Category
import ca.uwaterloo.dtqzzy.ece452.models.Subtask
import ca.uwaterloo.dtqzzy.ece452.models.Task
import ca.uwaterloo.dtqzzy.ece452.models.properties.JsonDeserializable
import com.google.gson.JsonObject
import com.google.gson.JsonParseException

@Entity(tableName = "task")
data class TaskEntity(
        @PrimaryKey(autoGenerate = true) var id: Long?,
        @ColumnInfo(name = "task_name") override var name: String,
        @ColumnInfo(name = "category") override var category: Category,
        @Embedded(prefix = "subtasks") override var subtasks: List<Subtask>
) : Task() {
    override fun toJson(): JsonObject {
        val jsonObject = super.toJson()
        jsonObject.addProperty("id", id)

        return jsonObject
    }

    companion object : JsonDeserializable<TaskEntity> {
        @JvmStatic
        override fun fromJson(jsonObject: JsonObject): TaskEntity = TaskEntity(
                jsonObject.get("id")?.asLong,
                jsonObject.get("name")?.asString
                        ?: throw JsonParseException("Json object has no name field or a null name"),
                CategoryEntity.fromJson(jsonObject.get("category").asJsonObject),
                jsonObject.get("subtasks").asJsonArray.map { jsonElement -> SubtaskEntity.fromJson(jsonElement.asJsonObject) })

        @JvmStatic
        override fun fromJson(jsonString: String): TaskEntity = super.fromJson(jsonString)
    }
}
