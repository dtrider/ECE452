package ca.uwaterloo.dtqzzy.ece452.models.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import ca.uwaterloo.dtqzzy.ece452.models.Event
import ca.uwaterloo.dtqzzy.ece452.models.RestrictionPeriod
import ca.uwaterloo.dtqzzy.ece452.models.properties.JsonDeserializable
import com.google.gson.JsonObject
import com.google.gson.JsonParseException
import org.threeten.bp.OffsetDateTime

@Entity(tableName = "event")
data class EventEntity(
        @PrimaryKey(autoGenerate = true) var id: Long?,
        @ColumnInfo(name = "name") override var name: String,
        @ColumnInfo(name = "source") override var source: String?,
        @ColumnInfo(name = "start_datetime") override var startDateTime: OffsetDateTime,
        @ColumnInfo(name = "end_datetime") override var endDateTime: OffsetDateTime
) : Event() {
    override fun toJson(): JsonObject {
        val jsonObject = super.toJson()
        jsonObject.addProperty("id", id)

        return jsonObject
    }

    companion object : JsonDeserializable<EventEntity>{
        @JvmStatic
        override fun fromJson(jsonObject: JsonObject): EventEntity = EventEntity(
                jsonObject.get("id")?.asLong,
                jsonObject.get("name")?.asString
                        ?: throw JsonParseException("Json object has no name field or a null name"),
                jsonObject.get("source")?.asString,
                OffsetDateTime.parse(jsonObject.get("startDateTime")?.asString),
                OffsetDateTime.parse(jsonObject.get("endDateTime")?.asString))

        @JvmStatic
        override fun fromJson(jsonString: String): EventEntity = super.fromJson(jsonString)
    }
}
