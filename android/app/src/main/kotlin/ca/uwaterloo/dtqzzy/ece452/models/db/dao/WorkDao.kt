package ca.uwaterloo.dtqzzy.ece452.models.db.dao

import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Update

interface WorkDao<in T> {
    @Insert
    fun create(entity: T)

    @Update
    fun update(entity: T)

    @Delete
    fun delete(entity: T)
}