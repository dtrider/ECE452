package ca.uwaterloo.dtqzzy.ece452.models.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import ca.uwaterloo.dtqzzy.ece452.models.db.TaskEntity

@Dao
abstract class TaskDao : WorkDao<TaskEntity> {
    @Query("SELECT * FROM task ORDER BY datetime(deadline)")
    abstract fun getTasks(): List<TaskEntity>

    @Query("DELETE FROM task")
    abstract fun deleteAll()
}
