package ca.uwaterloo.dtqzzy.ece452.models

import ca.uwaterloo.dtqzzy.ece452.models.properties.DateTimeRange
import ca.uwaterloo.dtqzzy.ece452.models.properties.JsonSerializable
import com.google.gson.JsonObject
import org.threeten.bp.OffsetDateTime
import org.threeten.bp.format.DateTimeFormatter

/**
 * A subtask that has been scheduled.
 */
abstract class ScheduledSubtask : DateTimeRange, JsonSerializable {
    abstract var subtask: Subtask
    abstract override var startDateTime: OffsetDateTime
    abstract override var endDateTime: OffsetDateTime

    override fun toJson(): JsonObject {
        val jsonObject = JsonObject()
        jsonObject.add("subtask", subtask.toJson())
        jsonObject.addProperty("startDateTime", startDateTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME))
        jsonObject.addProperty("endDateTime", endDateTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME))

        return jsonObject
    }
}
