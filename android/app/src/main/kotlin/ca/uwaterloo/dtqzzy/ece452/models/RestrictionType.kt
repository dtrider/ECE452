package ca.uwaterloo.dtqzzy.ece452.models

enum class RestrictionType(private val string: String) {
    BREAK_PERIOD("break_period"),
    BUSY_PERIOD("busy_period"),
    UNKNOWN("unknown");

    override fun toString(): String {
        return string
    }

    companion object {
        fun fromString(string: String?): RestrictionType {
            values().filter { it.string.equals(string, ignoreCase = true) }
                    .forEach { return it }
            return UNKNOWN
        }
    }
}