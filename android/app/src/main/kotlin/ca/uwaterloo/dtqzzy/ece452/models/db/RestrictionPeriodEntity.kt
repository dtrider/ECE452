package ca.uwaterloo.dtqzzy.ece452.models.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import ca.uwaterloo.dtqzzy.ece452.models.RestrictionPeriod
import ca.uwaterloo.dtqzzy.ece452.models.RestrictionType
import ca.uwaterloo.dtqzzy.ece452.models.properties.JsonDeserializable
import com.google.gson.JsonObject
import org.threeten.bp.OffsetDateTime

@Entity(tableName = "restriction_period")
data class RestrictionPeriodEntity(
        @PrimaryKey(autoGenerate = true) var id: Long?,
        @ColumnInfo(name = "restriction_type") override var type: RestrictionType,
        @ColumnInfo(name = "start_datetime") override var startDateTime: OffsetDateTime,
        @ColumnInfo(name = "end_datetime") override var endDateTime: OffsetDateTime
) : RestrictionPeriod() {
    override fun toJson(): JsonObject {
        val jsonObject = super.toJson()
        jsonObject.addProperty("id", id)

        return jsonObject
    }

    companion object : JsonDeserializable<RestrictionPeriodEntity> {
        @JvmStatic
        override fun fromJson(jsonObject: JsonObject): RestrictionPeriodEntity = RestrictionPeriodEntity(
                jsonObject.get("id")?.asLong,
                RestrictionType.fromString(jsonObject.get("type")?.asString),
                OffsetDateTime.parse(jsonObject.get("startDateTime")?.asString),
                OffsetDateTime.parse(jsonObject.get("endDateTime")?.asString))

        @JvmStatic
        override fun fromJson(jsonString: String): RestrictionPeriodEntity = super.fromJson(jsonString)
    }
}
