package ca.uwaterloo.dtqzzy.ece452.models.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import ca.uwaterloo.dtqzzy.ece452.models.db.CategoryEntity

@Dao
abstract class CategoryDao : WorkDao<CategoryEntity> {
    @Query("SELECT * FROM category ORDER BY name")
    abstract fun getCategories(): List<CategoryEntity>

    @Query("DELETE FROM category")
    abstract fun deleteAll()
}
