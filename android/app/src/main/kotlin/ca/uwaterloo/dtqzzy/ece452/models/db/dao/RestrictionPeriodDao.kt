package ca.uwaterloo.dtqzzy.ece452.models.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import ca.uwaterloo.dtqzzy.ece452.models.db.RestrictionPeriodEntity

@Dao
abstract class RestrictionPeriodDao : WorkDao<RestrictionPeriodEntity> {
    @Query("SELECT * FROM restriction_period ORDER BY datetime(start_datetime)")
    abstract fun getRestrictions(): List<RestrictionPeriodEntity>

    @Query("DELETE FROM restriction_period")
    abstract fun deleteAll()
}
