package ca.uwaterloo.dtqzzy.ece452.models.scheduler

import ca.uwaterloo.dtqzzy.ece452.models.properties.DateTimeRange
import org.threeten.bp.OffsetDateTime

data class FreeTime(
        override var startDateTime: OffsetDateTime,
        override var endDateTime: OffsetDateTime
) : DateTimeRange