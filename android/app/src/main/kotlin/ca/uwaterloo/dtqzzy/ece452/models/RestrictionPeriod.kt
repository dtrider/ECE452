package ca.uwaterloo.dtqzzy.ece452.models

import ca.uwaterloo.dtqzzy.ece452.models.properties.DateTimeRange
import ca.uwaterloo.dtqzzy.ece452.models.properties.JsonSerializable
import com.google.gson.JsonObject
import org.threeten.bp.OffsetDateTime
import org.threeten.bp.format.DateTimeFormatter

/**
 * A period of time where no tasks are allowed to be scheduled.
 * The type is provided to allow easy designation of what the restriction period is used for.
 */
abstract class RestrictionPeriod : DateTimeRange, JsonSerializable {
    abstract var type: RestrictionType
    abstract override var startDateTime: OffsetDateTime
    abstract override var endDateTime: OffsetDateTime

    override fun toJson(): JsonObject {
        val jsonObject = JsonObject()
        jsonObject.addProperty("type", type.toString())
        jsonObject.addProperty("startDateTime", startDateTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME))
        jsonObject.addProperty("endDateTime", endDateTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME))

        return jsonObject
    }
}
