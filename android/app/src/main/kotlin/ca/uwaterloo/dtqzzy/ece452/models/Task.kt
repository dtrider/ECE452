package ca.uwaterloo.dtqzzy.ece452.models

import ca.uwaterloo.dtqzzy.ece452.models.properties.JsonSerializable
import com.google.gson.JsonArray
import com.google.gson.JsonObject

/**
 * A subtask container.
 */
abstract class Task : JsonSerializable {
    abstract var name: String
    abstract var category: Category
    abstract var subtasks: List<Subtask>

    override fun toJson(): JsonObject {
        val jsonObject = JsonObject()
        jsonObject.addProperty("name", name)
        jsonObject.add("category", category.toJson())

        val subtaskArray = JsonArray()
        for (subtask in subtasks) {
            subtaskArray.add(subtask.toJson())
        }

        jsonObject.add("subtasks", subtaskArray)

        return jsonObject
    }
}
