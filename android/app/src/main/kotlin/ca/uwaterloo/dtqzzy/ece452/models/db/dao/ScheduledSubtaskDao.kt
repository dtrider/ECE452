package ca.uwaterloo.dtqzzy.ece452.models.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import ca.uwaterloo.dtqzzy.ece452.models.db.ScheduledSubtaskEntity

@Dao
abstract class ScheduledSubtaskDao : WorkDao<ScheduledSubtaskEntity> {
    @Query("SELECT * FROM scheduled_subtask ORDER BY datetime(start_datetime)")
    abstract fun getScheduledSubtasks(): List<ScheduledSubtaskEntity>

    @Query("DELETE FROM scheduled_subtask")
    abstract fun deleteAll()
}
