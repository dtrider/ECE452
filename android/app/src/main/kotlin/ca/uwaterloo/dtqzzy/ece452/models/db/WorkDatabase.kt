package ca.uwaterloo.dtqzzy.ece452.models.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context
import ca.uwaterloo.dtqzzy.ece452.models.db.converters.DurationConverter
import ca.uwaterloo.dtqzzy.ece452.models.db.converters.OffsetDateTimeConverter
import ca.uwaterloo.dtqzzy.ece452.models.db.converters.RestrictionTypeConverter
import ca.uwaterloo.dtqzzy.ece452.models.db.dao.*

@Database(entities = [
    CategoryEntity::class, EventEntity::class, RestrictionPeriodEntity::class, ScheduledSubtaskEntity::class, SubtaskEntity::class],
        version = 1,
        exportSchema = false)
@TypeConverters(DurationConverter::class, OffsetDateTimeConverter::class, RestrictionTypeConverter::class)
abstract class WorkDatabase : RoomDatabase() {
    abstract fun categoryDao(): CategoryDao
    abstract fun eventDao(): EventDao
    abstract fun restrictionPeriodDao(): RestrictionPeriodDao
    abstract fun scheduledSubtaskDao(): ScheduledSubtaskDao
    abstract fun subtaskDao(): SubtaskDao

    companion object {
        private var INSTANCE: WorkDatabase? = null

        @JvmStatic
        fun getInstance(context: Context): WorkDatabase? {
            if (INSTANCE == null) {
                synchronized(WorkDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            WorkDatabase::class.java, "work.db")
                            // TODO: Remove when Scheduler is asynchronous
                            .allowMainThreadQueries()
                            .build()
                }
            }
            return INSTANCE
        }

        @JvmStatic
        fun destroyInstance() {
            INSTANCE = null
        }
    }
}
