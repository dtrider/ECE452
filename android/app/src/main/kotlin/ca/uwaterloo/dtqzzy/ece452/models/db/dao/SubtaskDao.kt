package ca.uwaterloo.dtqzzy.ece452.models.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import ca.uwaterloo.dtqzzy.ece452.models.db.SubtaskEntity

@Dao
abstract class SubtaskDao : WorkDao<SubtaskEntity> {
    @Query("SELECT * FROM subtask ORDER BY datetime(deadline)")
    abstract fun getSubtasks(): List<SubtaskEntity>

    @Query("DELETE FROM subtask")
    abstract fun deleteAll()
}
