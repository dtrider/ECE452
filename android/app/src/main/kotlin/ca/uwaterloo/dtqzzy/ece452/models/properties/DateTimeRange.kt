package ca.uwaterloo.dtqzzy.ece452.models.properties

import org.threeten.bp.Duration
import org.threeten.bp.OffsetDateTime

/**
 * An interface that allows designates the implementation to have a  start and end date time range.
 *
 * Also provides a duration method that calculates this duration.
 */
interface DateTimeRange {
    var startDateTime: OffsetDateTime
    var endDateTime: OffsetDateTime

    val duration: Duration
        get() = Duration.between(startDateTime, endDateTime) ?: Duration.ZERO
}