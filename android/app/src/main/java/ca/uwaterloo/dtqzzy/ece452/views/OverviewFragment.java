package ca.uwaterloo.dtqzzy.ece452.views;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ca.uwaterloo.dtqzzy.ece452.MainApplication;
import ca.uwaterloo.dtqzzy.ece452.NavigationOption;
import ca.uwaterloo.dtqzzy.ece452.R;
import ca.uwaterloo.dtqzzy.ece452.helpers.DurationHelper;
import ca.uwaterloo.dtqzzy.ece452.models.Subtask;
import ca.uwaterloo.dtqzzy.ece452.models.db.SubtaskEntity;
import ca.uwaterloo.dtqzzy.ece452.schedulers.Scheduler;

public class OverviewFragment extends WorkFragment {

    private TextView userTasks;

    public OverviewFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_overview, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //For now, showing subtasks instead of tasks since we don't currently track tasks...
        userTasks = view.findViewById(R.id.tv_user_tasks);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final FloatingActionButton fab = getFloatingActionButton();
        if (fab != null) {
            fab.setVisibility(View.VISIBLE);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    navigate(new AddTaskFragment(), NavigationOption.ADD_TO_BACK_STACK);
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        final List<SubtaskEntity> subtasks = MainApplication.getDatabase().subtaskDao().getSubtasks();
        String displayText = "";

        for (Subtask subtask : subtasks) {
            displayText = displayText + subtask.getName() + ",    "
                    + DurationHelper.getFormattedDuration(subtask.getDuration())
                    + System.getProperty("line.separator");
        }

        userTasks.setText(displayText);
    }
}
