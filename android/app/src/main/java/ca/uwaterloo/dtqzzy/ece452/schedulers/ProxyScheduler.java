package ca.uwaterloo.dtqzzy.ece452.schedulers;

import android.support.annotation.NonNull;
import android.util.Log;

import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.temporal.ChronoUnit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import ca.uwaterloo.dtqzzy.ece452.models.Event;
import ca.uwaterloo.dtqzzy.ece452.models.RestrictionPeriod;
import ca.uwaterloo.dtqzzy.ece452.models.ScheduledSubtask;
import ca.uwaterloo.dtqzzy.ece452.models.Subtask;
import ca.uwaterloo.dtqzzy.ece452.schedulers.data.SchedulerData;

public class ProxyScheduler implements Scheduler {
    //@NonNull private List<RestrictionPeriod> restrictions = new ArrayList<>();
    //@NonNull private List<Event> events = new ArrayList<>();
    //@NonNull private List<Subtask> subtasks = new ArrayList<>();
    @NonNull private SchedulerResult schedulerResult = SchedulerResult.fail();
    @NonNull private List<ScheduledSubtask> scheduledSubtasks = new ArrayList<>();
    @NonNull private List<Subtask> unScheduledSubtasks = new ArrayList<>();
    @NonNull private final SchedulerData datastore;

    public ProxyScheduler(@NonNull SchedulerData datastore) {
        this.datastore = datastore;
    }

    private class NoSchedulePeriod {
        private OffsetDateTime startTime;
        private OffsetDateTime endTime;

        public NoSchedulePeriod(OffsetDateTime startTime, OffsetDateTime endTime) {
            this.startTime = startTime;
            this.endTime = endTime;
        }

        public OffsetDateTime getStartTime() {
            return startTime;
        }

        public OffsetDateTime getEndTime() {
            return endTime;
        }
    }


    @Override
    public SchedulerResult schedule() {
        final List<Subtask> subtasks = new ArrayList<>(datastore.getSubtasks());
        final List<Event> events = new ArrayList<>(datastore.getEvents());
        final List<RestrictionPeriod> restrictions = new ArrayList<>(datastore.getRestrictions());

        final List<ScheduledSubtask> results = new ArrayList<>();
        final List<NoSchedulePeriod> noSchedulePeriods = new ArrayList<>();

        final Map<OffsetDateTime, Long> freeTimes = new TreeMap<>();

        //sort subtasks by deadline
        Collections.sort(subtasks, new Comparator<Subtask>() {
            @Override
            public int compare(Subtask sub1, Subtask sub2) {
                return sub1.getDeadline().compareTo(sub2.getDeadline());
            }

        });

        // add events busy time
        for (Event ev : events) {
            noSchedulePeriods.add(new NoSchedulePeriod(ev.getStartDateTime(), ev.getEndDateTime()));
        }

        //add restriction busy time
        for (RestrictionPeriod re : restrictions) {
            noSchedulePeriods.add(new NoSchedulePeriod(re.getStartDateTime(), re.getEndDateTime()));
        }

        Collections.sort(noSchedulePeriods, new Comparator<NoSchedulePeriod>() {

            @Override
            public int compare(NoSchedulePeriod nsp1, NoSchedulePeriod nsp2) {
                return nsp1.getStartTime().compareTo(nsp2.getStartTime());
            }

        });

        OffsetDateTime firstFreeSlot = noSchedulePeriods.get(0).getEndTime();
        //get a list of free times
        for (int i = 1; i < noSchedulePeriods.size(); i++) {
            //opposite to busy time
            final OffsetDateTime start = noSchedulePeriods.get(i - 1).getEndTime();
            final OffsetDateTime end = noSchedulePeriods.get(i).getStartTime();
            final Long freeTime = start.until(end, ChronoUnit.MINUTES);

            freeTimes.put(start, freeTime);
        }

        //invoke validation
        if (validate(subtasks, freeTimes, firstFreeSlot) && freeTimes.size() > 0) {
            SubtaskScheduler subtaskScheduler = new SubtaskScheduler(subtasks, freeTimes);
            SchedulerResult result = subtaskScheduler.schedule();
            setCachedSchedule(result);
            return result;
        } else {
            //TODO: Display UI, update the return value
            SchedulerResult invalidResult = SchedulerResult.conflict(scheduledSubtasks, subtasks);
            setCachedSchedule(invalidResult);
            return invalidResult;
        }
    }


    @NonNull
    @Override
    public SchedulerResult getCachedSchedule() {
        return schedulerResult;
    }

    protected void setCachedSchedule(@NonNull SchedulerResult schedulerResult) {
        this.schedulerResult = schedulerResult;
    }

    private static boolean validate(List<Subtask> subtasks, Map<OffsetDateTime, Long> freeTimes, OffsetDateTime firstFreeSlot) {
        if(subtasks == null || subtasks.size() == 0) return false;

        //first deadline is before the first free time slot, ignore
        if (subtasks.get(0).getDeadline().isBefore(firstFreeSlot)) {
            return false;
        }

        //longest free time slot is shorter than a subtask duration
        long longestDuration = 0;
        for (long time : freeTimes.values()) {
            if (time > longestDuration) {
                longestDuration = time;
            }
        }

        for (Subtask subtask : subtasks) {
            if (subtask.getDuration().toMinutes() >= longestDuration) {
                return false;
            }
        }

        return true;
    }
}
