package ca.uwaterloo.dtqzzy.ece452.schedulers;

import android.support.annotation.NonNull;

import java.util.Collections;
import java.util.List;

import ca.uwaterloo.dtqzzy.ece452.models.ScheduledSubtask;
import ca.uwaterloo.dtqzzy.ece452.models.Subtask;

public class NullScheduler implements Scheduler {
    private final List<ScheduledSubtask> scheduledSubtasks = Collections.emptyList();

    public NullScheduler() {
    }

    @Override
    public SchedulerResult schedule() {
        return SchedulerResult.ok(scheduledSubtasks);
    }

    @NonNull
    @Override
    public SchedulerResult getCachedSchedule() {
        return SchedulerResult.ok(scheduledSubtasks);
    }
}
