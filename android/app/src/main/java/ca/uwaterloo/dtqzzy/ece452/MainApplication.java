package ca.uwaterloo.dtqzzy.ece452;

import android.app.Application;
import android.content.Context;

import com.jakewharton.threetenabp.AndroidThreeTen;

import ca.uwaterloo.dtqzzy.ece452.models.db.WorkDatabase;
import ca.uwaterloo.dtqzzy.ece452.schedulers.ProxyScheduler;
import ca.uwaterloo.dtqzzy.ece452.schedulers.data.DatabaseSchedulerData;
import ca.uwaterloo.dtqzzy.ece452.schedulers.Scheduler;

public class MainApplication extends Application {
    private static WorkDatabase database;
    private static Scheduler scheduler;

    public static Scheduler getScheduler() {
        return scheduler;
    }

    public static WorkDatabase getDatabase() {
        return database;
    }


    @Override
    public void onCreate() {
        super.onCreate();

        AndroidThreeTen.init(this);
        database = WorkDatabase.getInstance(this);
        scheduler = new ProxyScheduler(new DatabaseSchedulerData(database));
    }
}
