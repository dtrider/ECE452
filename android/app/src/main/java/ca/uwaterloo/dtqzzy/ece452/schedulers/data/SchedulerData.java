package ca.uwaterloo.dtqzzy.ece452.schedulers.data;

import java.util.List;

import ca.uwaterloo.dtqzzy.ece452.models.Event;
import ca.uwaterloo.dtqzzy.ece452.models.RestrictionPeriod;
import ca.uwaterloo.dtqzzy.ece452.models.ScheduledSubtask;
import ca.uwaterloo.dtqzzy.ece452.models.Subtask;

public interface SchedulerData {
    List<? extends RestrictionPeriod> getRestrictions();

    List<? extends Event> getEvents();

    List<? extends Subtask> getSubtasks();

    List<? extends ScheduledSubtask> getScheduledSubtasks();
}
