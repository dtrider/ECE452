/*
 * Note: This class is taken and modified from the Google WiFi Direct Demo at
 * https://android.googlesource.com/platform/development/+/master/samples/WiFiDirectDemo/
 */

package ca.uwaterloo.dtqzzy.ece452;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.List;

import ca.uwaterloo.dtqzzy.ece452.models.Event;
import ca.uwaterloo.dtqzzy.ece452.models.RestrictionPeriod;
import ca.uwaterloo.dtqzzy.ece452.models.ScheduledSubtask;
import ca.uwaterloo.dtqzzy.ece452.models.Subtask;
import ca.uwaterloo.dtqzzy.ece452.models.db.EventEntity;
import ca.uwaterloo.dtqzzy.ece452.models.db.RestrictionPeriodEntity;
import ca.uwaterloo.dtqzzy.ece452.models.db.ScheduledSubtaskEntity;
import ca.uwaterloo.dtqzzy.ece452.models.db.SubtaskEntity;
import ca.uwaterloo.dtqzzy.ece452.models.db.WorkDatabase;
import ca.uwaterloo.dtqzzy.ece452.schedulers.data.SchedulerData;
import ca.uwaterloo.dtqzzy.ece452.views.DeviceDetailFragment;

/**
 * A service that process each file transfer request i.e Intent by opening a
 * socket connection with the WiFi Direct Group Owner and writing the file
 */
public class FileTransferService extends IntentService {

    private static final String TAG = "FileTransferService";

    private static final int SOCKET_TIMEOUT = 5000;
    public static final String ACTION_SEND_FILE = "com.example.android.wifidirect.SEND_FILE";
    public static final String EXTRAS_FILE_PATH = "file_url";
    public static final String EXTRAS_GROUP_OWNER_ADDRESS = "go_host";
    public static final String EXTRAS_GROUP_OWNER_PORT = "go_port";

    public FileTransferService(String name) {
        super(name);
    }

    public FileTransferService() {
        super("FileTransferService");
    }

    /*
     * (non-Javadoc)
     * @see android.app.IntentService#onHandleIntent(android.content.Intent)
     */
    @Override
    protected void onHandleIntent(Intent intent) {

        Context context = getApplicationContext();
        if (intent.getAction().equals(ACTION_SEND_FILE)) {
//            String fileUri = intent.getExtras().getString(EXTRAS_FILE_PATH);
            String host = intent.getExtras().getString(EXTRAS_GROUP_OWNER_ADDRESS);
            Socket socket = new Socket();
            int port = intent.getExtras().getInt(EXTRAS_GROUP_OWNER_PORT);

            try {
                Log.d(FileTransferService.TAG, "Opening client socket - ");
                socket.bind(null);
                socket.connect((new InetSocketAddress(host, port)), SOCKET_TIMEOUT);

                Log.d(FileTransferService.TAG, "Client socket - " + socket.isConnected());
//                OutputStream stream = socket.getOutputStream();
//                ObjectOutputStream stream = new ObjectOutputStream(socket.getOutputStream());
                OutputStreamWriter stream = new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.UTF_8);
                WorkDatabase database = MainApplication.getDatabase();
//                List<SubtaskEntity> subtasks = database.subtaskDao().getSubtasks();
                List<EventEntity> events = database.eventDao().getEvents();
                List<RestrictionPeriodEntity> restrictionPeriods = database.restrictionPeriodDao().getRestrictions();
                List<ScheduledSubtaskEntity> scheduledSubtasks = database.scheduledSubtaskDao().getScheduledSubtasks();

                JsonObject dataToSend = new JsonObject();
                JsonArray eventArray = new JsonArray();
                JsonArray restrictionPeriodArray = new JsonArray();
                JsonArray scheduledSubtaskArray = new JsonArray();

                for (Event event : events){
                    eventArray.add(event.toJson());
                }
                for (RestrictionPeriod restrictionPeriod : restrictionPeriods){
                    restrictionPeriodArray.add(restrictionPeriod.toJson());
                }
                for (ScheduledSubtask scheduledSubtask : scheduledSubtasks){
                    scheduledSubtaskArray.add(scheduledSubtask.toJson());
                }

                dataToSend.add("events", eventArray);
                dataToSend.add("restrictionPeriods", restrictionPeriodArray);
                dataToSend.add("scheduledSubtasks", scheduledSubtaskArray);

                stream.write(dataToSend.toString());


                stream.close();
//                ContentResolver cr = context.getContentResolver();
//                InputStream is = null;
//                try {
//                    is = cr.openInputStream(Uri.parse(fileUri));
//                } catch (FileNotFoundException e) {
//                    Log.d(FileTransferService.TAG, e.toString());
//                }
//                DeviceDetailFragment.copyFile(is, stream);
                Log.d(FileTransferService.TAG, "Client: Data written");
            } catch (IOException e) {
                Log.e(FileTransferService.TAG, e.getMessage());
            } finally {
                if (socket != null) {
                    if (socket.isConnected()) {
                        try {
                            socket.close();
                        } catch (IOException e) {
                            // Give up
                            e.printStackTrace();
                        }
                    }
                }
            }

        }
    }
}
