package ca.uwaterloo.dtqzzy.ece452.views.external;


import org.threeten.bp.Duration;

import mobi.upod.timedurationpicker.TimeDurationPicker;
import mobi.upod.timedurationpicker.TimeDurationPickerDialogFragment;

public class DurationPickerDialogFragment extends TimeDurationPickerDialogFragment {
    private OnDurationSetListener durationSetListener = null;

    @Override
    public void onDurationSet(TimeDurationPicker view, long duration) {
        if (durationSetListener != null) {
            durationSetListener.onDurationSet(view, Duration.ofMillis(duration));
        }
    }

    public void setOnCompleteAction(OnDurationSetListener onDurationSetListener) {
        durationSetListener = onDurationSetListener;
    }

    public static abstract class OnDurationSetListener {
        public abstract void onDurationSet(TimeDurationPicker view, Duration duration);
    }
}
