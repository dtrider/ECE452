package ca.uwaterloo.dtqzzy.ece452.views.external;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

import org.threeten.bp.LocalDate;
import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.ZoneOffset;

import java.util.Calendar;

public class DatePickerDialogFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    private OnDateSetListener dateSetListener = null;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        return new DatePickerDialog(getActivity(),
                this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        if (dateSetListener != null) {
            dateSetListener.onDateSet(view, LocalDate.of(year, month + 1, day));
        }
    }

    public void setOnDateSetListener(OnDateSetListener onDateSetListener) {
        dateSetListener = onDateSetListener;
    }

    public static abstract class OnDateSetListener {
        public abstract void onDateSet(DatePicker view, LocalDate date);
    }
}
