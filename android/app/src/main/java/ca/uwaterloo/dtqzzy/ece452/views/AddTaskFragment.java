package ca.uwaterloo.dtqzzy.ece452.views;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import org.threeten.bp.Duration;
import org.threeten.bp.OffsetDateTime;

import java.util.ArrayList;
import java.util.List;

import ca.uwaterloo.dtqzzy.ece452.MainApplication;
import ca.uwaterloo.dtqzzy.ece452.R;
import ca.uwaterloo.dtqzzy.ece452.adapters.AddSubtaskListAdapter;
import ca.uwaterloo.dtqzzy.ece452.models.db.SubtaskEntity;

public class AddTaskFragment extends WorkFragment {

    private LinearLayout layoutAddSubtask;
    private Button saveTaskButton;
    private ListView listView;
    private AddSubtaskListAdapter listViewAdapter;

    public AddTaskFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_add_task, container, false);
    }

    private void onAddSubTaskButtonClicked() {
        // The actual string value passed in here here doesn't matter
        // maybe I'll use that for something eventually, or just remove it
        listViewAdapter.add(new SubtaskEntity(null, "", Duration.ZERO, OffsetDateTime.MIN));
    }

    private void onSaveTaskButtonClicked() {
        final List<SubtaskEntity> subtasksToPersist = new ArrayList<>();

        // Check user input and add to list
        // Probably not safe, if the user modifies the entries when saving
        for (int i = 0; i < listViewAdapter.getCount(); i++) {
            final SubtaskEntity subtaskEntity = (SubtaskEntity) listViewAdapter.getItem(i);

            if (subtaskEntity != null) {
                if ("".equals(subtaskEntity.getName())) {
                    Toast.makeText(getActivity(), "Please enter a name for all subtasks",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                if (Duration.ZERO.equals(subtaskEntity.getDuration())) {
                    Toast.makeText(getActivity(), "Please enter a duration for all subtasks",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                if (OffsetDateTime.MIN.equals(subtaskEntity.getDeadline())) {
                    Toast.makeText(getActivity(), "Please enter a deadline for all subtasks",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                subtasksToPersist.add(subtaskEntity);
            }
        }

        for (SubtaskEntity entity : subtasksToPersist) {
            MainApplication.getDatabase().subtaskDao().create(entity);
        }

        Toast.makeText(getActivity(), "Subtasks saved.",
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final List<SubtaskEntity> subtaskEntities = new ArrayList<>();
        listViewAdapter = new AddSubtaskListAdapter(getActivity(), subtaskEntities);

        listView = view.findViewById(R.id.list_subtasks);
        listView.setAdapter(listViewAdapter);

        layoutAddSubtask = view.findViewById(R.id.layout_add_subtask);
        layoutAddSubtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onAddSubTaskButtonClicked();
            }
        });

        saveTaskButton = view.findViewById(R.id.btn_save_task);
        saveTaskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSaveTaskButtonClicked();
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final FloatingActionButton fab = getFloatingActionButton();
        if (fab != null) {
            fab.setVisibility(View.GONE);
        }
    }
}
