package ca.uwaterloo.dtqzzy.ece452.views;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.temporal.ChronoUnit;

import java.util.ArrayList;
import java.util.List;

import ca.uwaterloo.dtqzzy.ece452.MainApplication;
import ca.uwaterloo.dtqzzy.ece452.R;
import ca.uwaterloo.dtqzzy.ece452.adapters.CalendarEventListAdapter;
import ca.uwaterloo.dtqzzy.ece452.adapters.CalendarSubtaskListAdapter;
import ca.uwaterloo.dtqzzy.ece452.models.RestrictionType;
import ca.uwaterloo.dtqzzy.ece452.models.ScheduledSubtask;
import ca.uwaterloo.dtqzzy.ece452.models.db.EventEntity;
import ca.uwaterloo.dtqzzy.ece452.models.db.RestrictionPeriodEntity;
import ca.uwaterloo.dtqzzy.ece452.models.db.dao.RestrictionPeriodDao;
import ca.uwaterloo.dtqzzy.ece452.notification.NotificationHandler;
import ca.uwaterloo.dtqzzy.ece452.schedulers.SchedulerResult;

public class CalendarFragment extends WorkFragment {

    private Button scheduleButton;

    private CalendarEventListAdapter eventListAdapter;
    private CalendarSubtaskListAdapter subtaskListAdapter;

    private ListView eventListView;
    private ListView subtaskListView;

    public CalendarFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_calendar, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final List<EventEntity> events = new ArrayList<>(MainApplication.getDatabase().eventDao().getEvents());
        final SchedulerResult schedulerResult = MainApplication.getScheduler().getCachedSchedule();
        final List<ScheduledSubtask> scheduledSubtasks = new ArrayList<>(schedulerResult.getScheduledSubtasks());

        eventListAdapter = new CalendarEventListAdapter(getActivity(), events);
        subtaskListAdapter = new CalendarSubtaskListAdapter(getActivity(), scheduledSubtasks);

        eventListView = view.findViewById(R.id.list_events);
        subtaskListView = view.findViewById(R.id.list_subtasks);

        eventListView.setAdapter(eventListAdapter);
        subtaskListView.setAdapter(subtaskListAdapter);

        scheduleButton = view.findViewById(R.id.btn_schedule);
        scheduleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onScheduleClicked();
            }
        });

        Button clean = view.findViewById(R.id.btn_clean);
        clean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCleanClicked();
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final FloatingActionButton fab = getFloatingActionButton();
        if (fab != null) {
            fab.setVisibility(View.GONE);
        }
    }

    private void populateRestrictions(RestrictionPeriodDao restrictionDao) {
        MainApplication.getDatabase().restrictionPeriodDao().deleteAll();
        int day = OffsetDateTime.now().getDayOfMonth();
        int month = OffsetDateTime.now().getMonthValue();
        //For sleep time
        OffsetDateTime current = OffsetDateTime.of(2018, month, day, 23, 00, 0, 0, ZoneOffset.UTC);
        OffsetDateTime end = OffsetDateTime.of(2018, 4, 30, 00, 00, 0, 0, ZoneOffset.UTC);
        while (current.isBefore(end)) {
            restrictionDao.create(new RestrictionPeriodEntity(
                    null,
                    RestrictionType.BREAK_PERIOD,
                    current,
                    current.plus(9, ChronoUnit.HOURS)
            ));
            current = current.plusDays(1);
        }

        //For start time
        if(OffsetDateTime.now().isAfter(OffsetDateTime.of(2018, month, day, 8, 00, 0, 0, ZoneOffset.UTC))
                &&OffsetDateTime.now().isBefore(OffsetDateTime.of(2018, month, day, 23, 00, 0, 0, ZoneOffset.UTC))){
            int hour = OffsetDateTime.now().getHour();
            int minute = OffsetDateTime.now().getMinute();
            restrictionDao.create(new RestrictionPeriodEntity(
                    null,
                    RestrictionType.BREAK_PERIOD,
                    OffsetDateTime.of(2018, month, day, hour, minute, 0, 0, ZoneOffset.UTC),
                    OffsetDateTime.of(2018, month, day, hour, minute, 0, 0, ZoneOffset.UTC).plus(1, ChronoUnit.MINUTES)
            ));
        }

        //For end time
        restrictionDao.create(new RestrictionPeriodEntity(
                null,
                RestrictionType.BREAK_PERIOD,
                OffsetDateTime.of(2018, 4, 30, 11, 0, 0, 0, ZoneOffset.UTC),
                OffsetDateTime.of(2018, 4, 30, 12, 45, 0, 0, ZoneOffset.UTC)
        ));
    }

    private void onCleanClicked() {
        MainApplication.getDatabase().eventDao().deleteAll();
        MainApplication.getDatabase().subtaskDao().deleteAll();
        MainApplication.getDatabase().scheduledSubtaskDao().deleteAll();

        // TODO: Put this on a worker thread
        eventListAdapter.clear();

        // TODO: Put this on a worker thread
        subtaskListAdapter.clear();
    }

    private void onScheduleClicked() {
        populateRestrictions(MainApplication.getDatabase().restrictionPeriodDao());
        SchedulerResult result = MainApplication.getScheduler().schedule();

        //Setup Notification
        NotificationHandler nh = new NotificationHandler();
        nh.scheduleNotification(getActivity(), result.getScheduledSubtasks());

        // TODO: Put this on a worker thread
        eventListAdapter.clear();
        eventListAdapter.addAll(MainApplication.getDatabase().eventDao().getEvents());

        // TODO: Put this on a worker thread
        subtaskListAdapter.clear();
        subtaskListAdapter.addAll(MainApplication.getScheduler().getCachedSchedule().getScheduledSubtasks());
    }
}
