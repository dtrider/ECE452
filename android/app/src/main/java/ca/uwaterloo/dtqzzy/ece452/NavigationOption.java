package ca.uwaterloo.dtqzzy.ece452;

public enum NavigationOption {
    ADD_TO_BACK_STACK,
    REPLACE_CURRENT
}