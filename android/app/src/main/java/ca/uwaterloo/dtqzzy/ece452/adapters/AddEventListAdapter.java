package ca.uwaterloo.dtqzzy.ece452.adapters;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import org.threeten.bp.Duration;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalTime;
import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.List;

import ca.uwaterloo.dtqzzy.ece452.R;
import ca.uwaterloo.dtqzzy.ece452.models.Event;
import ca.uwaterloo.dtqzzy.ece452.models.Subtask;
import ca.uwaterloo.dtqzzy.ece452.views.external.DatePickerDialogFragment;
import ca.uwaterloo.dtqzzy.ece452.views.external.DurationPickerDialogFragment;
import ca.uwaterloo.dtqzzy.ece452.views.external.TimePickerDialogFragment;
import mobi.upod.timedurationpicker.TimeDurationPicker;


public class AddEventListAdapter<T extends Event> extends ArrayAdapter<T> {
    public AddEventListAdapter(Activity context, List<T> events) {
        super(context, 0, events);
    }

    private String getFormattedDuration(Duration duration) {
        final StringBuilder builder = new StringBuilder();

        if (duration.toHours() > 0) {
            builder.append(duration.toHours());
            builder.append(" hours ");
        }

        if (duration.toMinutes() > 0) {
            builder.append(duration.toMinutes() - duration.toHours() * 60);
            builder.append(" minutes ");
        }

        if (duration.toMillis() < 60000) {
            builder.append("under a minute");
        }

        return builder.toString().trim();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_add_event, parent, false);
        }

        final Event event = getItem(position);
        if (event == null) {
            return convertView;
        }

        final EditText eventName = convertView.findViewById(R.id.et_event_name);
        final TextView eventStart = convertView.findViewById(R.id.tv_event_start_datetime);
        final TextView eventEnd = convertView.findViewById(R.id.tv_event_end_datetime);
        final ImageView deleteEvent = convertView.findViewById(R.id.iv_delete_event);

        eventName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                event.setName(editable.toString());
            }
        });

        eventStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final DatePickerDialogFragment datePicker = new DatePickerDialogFragment();
                datePicker.setOnDateSetListener(new DatePickerDialogFragment.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, final LocalDate date) {
                        final TimePickerDialogFragment timePicker = new TimePickerDialogFragment();
                        timePicker.setOnTimeSetListener(new TimePickerDialogFragment.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, LocalTime time) {
                                final OffsetDateTime startDateTime = time.atOffset(ZoneOffset.UTC).atDate(date);
                                event.setStartDateTime(startDateTime);
                                eventStart.setText(event.getStartDateTime().format(DateTimeFormatter.ofPattern("MM/dd hh:mm a")));
                            }
                        });
                        timePicker.show(((FragmentActivity) getContext()).getFragmentManager(), "time");

                    }
                });
                datePicker.show(((FragmentActivity) getContext()).getFragmentManager(), "date");
            }
        });

        eventEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final DatePickerDialogFragment datePicker = new DatePickerDialogFragment();
                datePicker.setOnDateSetListener(new DatePickerDialogFragment.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, final LocalDate date) {
                        final TimePickerDialogFragment timePicker = new TimePickerDialogFragment();
                        timePicker.setOnTimeSetListener(new TimePickerDialogFragment.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, LocalTime time) {
                                final OffsetDateTime endDateTime = time.atOffset(ZoneOffset.UTC).atDate(date);
                                event.setEndDateTime(endDateTime);
                                eventEnd.setText(event.getEndDateTime().format(DateTimeFormatter.ofPattern("MM/dd hh:mm a")));
                            }
                        });
                        timePicker.show(((FragmentActivity) getContext()).getFragmentManager(), "time");

                    }
                });
                datePicker.show(((FragmentActivity) getContext()).getFragmentManager(), "date");
            }
        });

        deleteEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                remove((T) event);
            }
        });

        return convertView;

    }
}
