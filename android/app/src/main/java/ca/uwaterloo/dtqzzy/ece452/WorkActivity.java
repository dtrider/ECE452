package ca.uwaterloo.dtqzzy.ece452;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import ca.uwaterloo.dtqzzy.ece452.views.WorkFragment;

public abstract class WorkActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @NonNull
    public abstract FloatingActionButton getFloatingActionButton();

    public void navigate(@Nullable WorkFragment fragment, @NonNull NavigationOption option) {
        if (fragment == null) {
            return;
        }

        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (option) {
            case REPLACE_CURRENT:
                transaction.replace(R.id.main_container, fragment);
                break;

            case ADD_TO_BACK_STACK:
                transaction.replace(R.id.main_container, fragment)
                        .addToBackStack(fragment.getTag());
                break;
        }
        transaction.commit();
    }
}
