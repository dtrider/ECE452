/*
 * Note: Parts of this class are taken and modified from the Google WiFi Direct Demo at
 * https://android.googlesource.com/platform/development/+/master/samples/WiFiDirectDemo/
 */

package ca.uwaterloo.dtqzzy.ece452;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.ChannelListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import org.threeten.bp.OffsetDateTime;

import java.util.ArrayList;
import java.util.List;

import ca.uwaterloo.dtqzzy.ece452.adapters.FreeTimeListAdapter;
import ca.uwaterloo.dtqzzy.ece452.models.db.EventEntity;
import ca.uwaterloo.dtqzzy.ece452.models.db.RestrictionPeriodEntity;
import ca.uwaterloo.dtqzzy.ece452.models.db.ScheduledSubtaskEntity;
import ca.uwaterloo.dtqzzy.ece452.models.db.WorkDatabase;
import ca.uwaterloo.dtqzzy.ece452.models.scheduler.FreeTime;
import ca.uwaterloo.dtqzzy.ece452.schedulers.FreeTimeScheduler;
import ca.uwaterloo.dtqzzy.ece452.views.DeviceDetailFragment;
import ca.uwaterloo.dtqzzy.ece452.views.DeviceListFragment;
import ca.uwaterloo.dtqzzy.ece452.views.DeviceListFragment.DeviceActionListener;

public class ShareActivity extends WorkActivity implements ChannelListener, DeviceActionListener {

    private Button findPeersButton;
    private Button findFreeTimesButton;

    private FreeTimeListAdapter freeTimeListAdapter;
    private ListView freeTimesListView;

    private boolean isWifiP2pEnabled = false;
    private boolean retryChannel = false;

    private WifiP2pManager manager;
    private WifiP2pManager.Channel channel;
    private BroadcastReceiver receiver = null;
    private final IntentFilter intentFilter = new IntentFilter();

    private static final String TAG = "ShareActivity";

    public List<List<EventEntity>> otherPeersEvents = new ArrayList<>();
    public List<List<RestrictionPeriodEntity>> otherPeersRestrictionPeriods = new ArrayList<>();
    public List<List<ScheduledSubtaskEntity>> otherPeersScheduledSubtasks = new ArrayList<>();

    /**
     * @param isWifiP2pEnabled the isWifiP2pEnabled to set
     */
    public void setIsWifiP2pEnabled(boolean isWifiP2pEnabled) {
        this.isWifiP2pEnabled = isWifiP2pEnabled;
    }

    public void resetData() {
        DeviceListFragment fragmentList = (DeviceListFragment) getFragmentManager()
                .findFragmentById(R.id.frag_list);
        DeviceDetailFragment fragmentDetails = (DeviceDetailFragment) getFragmentManager()
                .findFragmentById(R.id.frag_detail);
        if (fragmentList != null) {
            fragmentList.clearPeers();
        }
        if (fragmentDetails != null) {
            fragmentDetails.resetViews();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        freeTimeListAdapter = new FreeTimeListAdapter(this, new ArrayList<FreeTime>());
        freeTimesListView = findViewById(R.id.list_free_times);
        freeTimesListView.setAdapter(freeTimeListAdapter);

        findPeersButton = findViewById(R.id.btn_find_peers);
        findPeersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onFindPeersClicked();
            }
        });

        findFreeTimesButton = findViewById(R.id.btn_find_free_times);
        findFreeTimesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onFindFreeTimesClicked();
            }
        });

        manager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        channel = manager.initialize(this, getMainLooper(), null);
        receiver = new WiFiDirectBroadcastReceiver(manager, channel, this);

        intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
//        registerReceiver(receiver, intentFilter);

    }

    /**
     * register the BroadcastReceiver with the intent values to be matched
     */
    @Override
    public void onResume() {
        super.onResume();
        receiver = new WiFiDirectBroadcastReceiver(manager, channel, this);
        registerReceiver(receiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    public void onChannelDisconnected() {
        // we will try once more
        if (manager != null && !retryChannel) {
            Toast.makeText(this, "Channel lost. Trying again", Toast.LENGTH_LONG).show();
            resetData();
            retryChannel = true;
            manager.initialize(this, getMainLooper(), this);
        } else {
            Toast.makeText(this,
                    "Severe! Channel is probably lost permanently. Try Disable/Re-Enable P2P.",
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void showDetails(WifiP2pDevice device) {
        DeviceDetailFragment fragment = (DeviceDetailFragment) getFragmentManager()
                .findFragmentById(R.id.frag_detail);
        fragment.showDetails(device);

    }

    @Override
    public void connect(WifiP2pConfig config) {
        manager.connect(channel, config, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
                // WiFiDirectBroadcastReceiver will notify us. Ignore for now.
            }

            @Override
            public void onFailure(int reason) {
                Toast.makeText(ShareActivity.this, "Connect failed. Retry.",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void disconnect(final Boolean justReceivedData) {
        final DeviceDetailFragment fragment = (DeviceDetailFragment) getFragmentManager()
                .findFragmentById(R.id.frag_detail);
        fragment.resetViews();
        manager.removeGroup(channel, new WifiP2pManager.ActionListener() {

            @Override
            public void onFailure(int reasonCode) {
                Log.d(ShareActivity.TAG, "Disconnect failed. Reason :" + reasonCode);

            }

            @Override
            public void onSuccess() {
                fragment.getView().setVisibility(View.GONE);

                if (justReceivedData) {

                    Toast.makeText(ShareActivity.this, "Data transmission successful.",
                            Toast.LENGTH_LONG).show();
                }
            }

        });

    }

    @Override
    public void cancelDisconnect() {

        /*
         * A cancel abort request by user. Disconnect i.e. removeGroup if
         * already connected. Else, request WifiP2pManager to abort the ongoing
         * request
         */
        if (manager != null) {
            final DeviceListFragment fragment = (DeviceListFragment) getFragmentManager()
                    .findFragmentById(R.id.frag_list);
            if (fragment.getDevice() == null
                    || fragment.getDevice().status == WifiP2pDevice.CONNECTED) {
                disconnect(false);
            } else if (fragment.getDevice().status == WifiP2pDevice.AVAILABLE
                    || fragment.getDevice().status == WifiP2pDevice.INVITED) {

                manager.cancelConnect(channel, new WifiP2pManager.ActionListener() {

                    @Override
                    public void onSuccess() {
                        Toast.makeText(ShareActivity.this, "Aborting connection",
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(int reasonCode) {
                        Toast.makeText(ShareActivity.this,
                                "Connect abort request failed. Reason Code: " + reasonCode,
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }

    }

    private void onFindPeersClicked() {

        manager.discoverPeers(channel, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
                Toast.makeText(ShareActivity.this, "Discovery Initiated",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int reasonCode) {
                Toast.makeText(ShareActivity.this, "Discovery Failed: " + reasonCode,
                        Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void onFindFreeTimesClicked() {


        List<List<EventEntity>> events = new ArrayList<List<EventEntity>>();
        List<List<RestrictionPeriodEntity>> restrictionPeriods = new ArrayList<List<RestrictionPeriodEntity>>();
        List<List<ScheduledSubtaskEntity>> scheduledSubtasks = new ArrayList<List<ScheduledSubtaskEntity>>();

        WorkDatabase database = MainApplication.getDatabase();

        events.add(database.eventDao().getEvents());
        events.addAll(otherPeersEvents);

        restrictionPeriods.add(database.restrictionPeriodDao().getRestrictions());
        restrictionPeriods.addAll(otherPeersRestrictionPeriods);

        scheduledSubtasks.add(database.scheduledSubtaskDao().getScheduledSubtasks());
        scheduledSubtasks.addAll(otherPeersScheduledSubtasks);

        List<List<OffsetDateTime[]>> busyLists = new ArrayList<List<OffsetDateTime[]>>();

        for (int i = 0; i < events.size(); i++) { //All 3 arrays should have the same length
            busyLists.add(new ArrayList<OffsetDateTime[]>());

            for (EventEntity event : events.get(i)) {
                busyLists.get(i).add(new OffsetDateTime[]{event.getStartDateTime(), event.getEndDateTime()});
            }
            for (RestrictionPeriodEntity restrictionPeriodEntity : restrictionPeriods.get(i)) {
                busyLists.get(i).add(new OffsetDateTime[]{restrictionPeriodEntity.getStartDateTime(), restrictionPeriodEntity.getEndDateTime()});
            }
            for (ScheduledSubtaskEntity scheduledSubtaskEntity : scheduledSubtasks.get(i)) {
                busyLists.get(i).add(new OffsetDateTime[]{scheduledSubtaskEntity.getStartDateTime(), scheduledSubtaskEntity.getEndDateTime()});
            }

        }

        final FreeTimeScheduler freeTimeScheduler = new FreeTimeScheduler(busyLists);
        final List<FreeTime> freeTimes = freeTimeScheduler.findFreeTimes();

        freeTimeListAdapter.clear();
        freeTimeListAdapter.addAll(freeTimes);
    }

    //This will fail - currently there is no floating action button
    @NonNull
    @Override
    public FloatingActionButton getFloatingActionButton() {
        return findViewById(R.id.fab);
    }

}
