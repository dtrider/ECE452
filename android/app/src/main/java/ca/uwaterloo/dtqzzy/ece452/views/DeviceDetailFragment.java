/*
 * Note: This class is taken and modified from the Google WiFi Direct Demo at
 * https://android.googlesource.com/platform/development/+/master/samples/WiFiDirectDemo/
 */

package ca.uwaterloo.dtqzzy.ece452.views;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager.ConnectionInfoListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import ca.uwaterloo.dtqzzy.ece452.FileTransferService;
import ca.uwaterloo.dtqzzy.ece452.MainApplication;
import ca.uwaterloo.dtqzzy.ece452.ShareActivity;
import ca.uwaterloo.dtqzzy.ece452.models.Event;
import ca.uwaterloo.dtqzzy.ece452.models.ScheduledSubtask;
import ca.uwaterloo.dtqzzy.ece452.models.db.EventEntity;
import ca.uwaterloo.dtqzzy.ece452.models.db.RestrictionPeriodEntity;
import ca.uwaterloo.dtqzzy.ece452.models.db.ScheduledSubtaskEntity;
import ca.uwaterloo.dtqzzy.ece452.models.db.SubtaskEntity;
import ca.uwaterloo.dtqzzy.ece452.models.db.WorkDatabase;
import ca.uwaterloo.dtqzzy.ece452.views.DeviceListFragment.DeviceActionListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;

import ca.uwaterloo.dtqzzy.ece452.R;

/**
 * A fragment that manages a particular peer and allows interaction with device
 * i.e. setting up network connection and transferring data.
 */
public class DeviceDetailFragment extends Fragment implements ConnectionInfoListener {

    protected static final int CHOOSE_FILE_RESULT_CODE = 20;
    private static View mContentView = null;
    private WifiP2pDevice device;
    private WifiP2pInfo info;
    ProgressDialog progressDialog = null;

    private static final String TAG = "DeviceDetailFragment";

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mContentView = inflater.inflate(R.layout.device_detail, null);
        mContentView.findViewById(R.id.btn_connect).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                WifiP2pConfig config = new WifiP2pConfig();
                config.deviceAddress = device.deviceAddress;
                config.wps.setup = WpsInfo.PBC;
                config.groupOwnerIntent = 15; //Make this device the group owner every time
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                progressDialog = ProgressDialog.show(getActivity(), "Press back to cancel",
                        "Connecting to :" + device.deviceAddress, true, true
//                        new DialogInterface.OnCancelListener() {
//
//                            @Override
//                            public void onCancel(DialogInterface dialog) {
//                                ((DeviceActionListener) getActivity()).cancelDisconnect();
//                            }
//                        }
                        );
                ((DeviceActionListener) getActivity()).connect(config);

            }
        });

        mContentView.findViewById(R.id.btn_disconnect).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        ((DeviceActionListener) getActivity()).disconnect(false);
                    }
                });
        //Sends the calendar
        mContentView.findViewById(R.id.btn_start_client).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Intent serviceIntent = new Intent(getActivity(), FileTransferService.class);
                        serviceIntent.setAction(FileTransferService.ACTION_SEND_FILE);
                        serviceIntent.putExtra(FileTransferService.EXTRAS_GROUP_OWNER_ADDRESS,
                                info.groupOwnerAddress.getHostAddress());
                        serviceIntent.putExtra(FileTransferService.EXTRAS_GROUP_OWNER_PORT, 8988);
                        getActivity().startService(serviceIntent);
                    }
                });

        return mContentView;
    }


    @Override
    public void onConnectionInfoAvailable(final WifiP2pInfo info) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        this.info = info;
        this.getView().setVisibility(View.VISIBLE);

        // The owner IP is now known.
        TextView view = (TextView) mContentView.findViewById(R.id.group_owner);
        view.setText(getResources().getString(R.string.group_owner_text)
                + ((info.isGroupOwner == true) ? getResources().getString(R.string.yes)
                        : getResources().getString(R.string.no)));

        // InetAddress from WifiP2pInfo struct.
        view = (TextView) mContentView.findViewById(R.id.device_info);
        view.setText("Group Owner IP - " + info.groupOwnerAddress.getHostAddress());

        // After the group negotiation, we assign the group owner as the file
        // server. The file server is single threaded, single connection server
        // socket.
        if (info.groupFormed && info.isGroupOwner) {
            Log.d(DeviceDetailFragment.TAG, "Server: About to start AsyncTask");
            new FileServerAsyncTask((ShareActivity)getActivity(), mContentView.findViewById(R.id.status_text))
                    .execute();
        } else if (info.groupFormed) {
            // The other device acts as the client. In this case, we enable the
            // get file button.
            mContentView.findViewById(R.id.btn_start_client).setVisibility(View.VISIBLE);
            ((TextView) mContentView.findViewById(R.id.status_text)).setText(getResources()
                    .getString(R.string.client_text));
        }

        // hide the connect button
        mContentView.findViewById(R.id.btn_connect).setVisibility(View.GONE);
    }

    /**
     * Updates the UI with device data
     * 
     * @param device the device to be displayed
     */
    public void showDetails(WifiP2pDevice device) {
        this.device = device;
        this.getView().setVisibility(View.VISIBLE);
        TextView view = (TextView) mContentView.findViewById(R.id.device_address);
        view.setText(device.deviceAddress);
        view = (TextView) mContentView.findViewById(R.id.device_info);
        view.setText(device.toString());

    }

    /**
     * Clears the UI fields after a disconnect or direct mode disable operation.
     */
    public void resetViews() {
        mContentView.findViewById(R.id.btn_connect).setVisibility(View.VISIBLE);
        TextView view = (TextView) mContentView.findViewById(R.id.device_address);
        view.setText(R.string.empty);
        view = (TextView) mContentView.findViewById(R.id.device_info);
        view.setText(R.string.empty);
        view = (TextView) mContentView.findViewById(R.id.group_owner);
        view.setText(R.string.empty);
        view = (TextView) mContentView.findViewById(R.id.status_text);
        view.setText(R.string.empty);
        mContentView.findViewById(R.id.btn_start_client).setVisibility(View.GONE);
        this.getView().setVisibility(View.GONE);
    }

    //Calendar receiving done in here
    public static class FileServerAsyncTask extends AsyncTask<Void, Void, String> {

        private ShareActivity parentShareActivity;
        private TextView statusText;


        public FileServerAsyncTask(ShareActivity parentShareActivity, View statusText) {
            this.parentShareActivity = parentShareActivity;
            this.statusText = (TextView) statusText;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                ServerSocket serverSocket = new ServerSocket(8988);
                Log.d(DeviceDetailFragment.TAG, "Server: Socket opened");
                Socket client = serverSocket.accept();
                Log.d(DeviceDetailFragment.TAG, "Server: connection done");
                Reader r = new InputStreamReader(client.getInputStream(), StandardCharsets.UTF_8);
                char[] buffer = new char[1024];
                StringBuilder sb = new StringBuilder();
                for(int len; (len = r.read(buffer)) > 0;)
                    sb.append(buffer, 0, len);

                String dataString = sb.toString();

                Gson gson = new Gson();

                JsonObject dataJson = gson.fromJson(dataString, JsonObject.class);

                JsonArray eventArray = dataJson.getAsJsonArray("events");
                JsonArray restrictionPeriodArray = dataJson.getAsJsonArray("restrictionPeriods");
                JsonArray scheduledSubtaskArray = dataJson.getAsJsonArray("scheduledSubtasks");

                LinkedList<EventEntity> eventsOtherPeer = new LinkedList<>();
                LinkedList<RestrictionPeriodEntity> restrictionPeriodsOtherPeer = new LinkedList<>();
                LinkedList<ScheduledSubtaskEntity> scheduledSubtasksOtherPeer = new LinkedList<>();

                for (JsonElement eventJson: eventArray){
                    JsonObject eventJsonObject = eventJson.getAsJsonObject();
                    EventEntity event = EventEntity.fromJson(eventJsonObject);

                    eventsOtherPeer.add(event);

                }

                for (JsonElement restrictionPeriodJson: restrictionPeriodArray){
                    JsonObject restrictionPeriodJsonObject = restrictionPeriodJson.getAsJsonObject();
                    RestrictionPeriodEntity restrictionPeriod = RestrictionPeriodEntity.fromJson(restrictionPeriodJsonObject);

                    restrictionPeriodsOtherPeer.add(restrictionPeriod);

                }

                for (JsonElement scheduledSubtaskJson: scheduledSubtaskArray){
                    JsonObject scheduledSubtaskJsonObject = scheduledSubtaskJson.getAsJsonObject();
                    ScheduledSubtaskEntity scheduledSubtask = ScheduledSubtaskEntity.fromJson(scheduledSubtaskJsonObject);

                    scheduledSubtasksOtherPeer.add(scheduledSubtask);

                }

                parentShareActivity.otherPeersEvents.add(eventsOtherPeer);
                parentShareActivity.otherPeersRestrictionPeriods.add(restrictionPeriodsOtherPeer);
                parentShareActivity.otherPeersScheduledSubtasks.add(scheduledSubtasksOtherPeer);
                serverSocket.close();

                return "received data";

            } catch (Exception e) {
                Log.e(DeviceDetailFragment.TAG, e.getMessage());
                return null;
            }
        }


        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                statusText.setText("Received data");

                parentShareActivity.disconnect(true);
            }

        }


        @Override
        protected void onPreExecute() {
            statusText.setText("Opening a server socket");
        }

    }

}
