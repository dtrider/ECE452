package ca.uwaterloo.dtqzzy.ece452.views;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import org.threeten.bp.Duration;
import org.threeten.bp.OffsetDateTime;

import java.util.ArrayList;
import java.util.List;

import ca.uwaterloo.dtqzzy.ece452.MainApplication;
import ca.uwaterloo.dtqzzy.ece452.R;
import ca.uwaterloo.dtqzzy.ece452.adapters.AddEventListAdapter;
import ca.uwaterloo.dtqzzy.ece452.models.db.EventEntity;

public class AddEventFragment extends WorkFragment {

    private LinearLayout layoutAddEvent;
    private Button saveTaskButton;
    private ListView listView;
    private AddEventListAdapter listViewAdapter;

    public AddEventFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_add_event, container, false);
    }

    private void onAddEventButtonClicked() {
        // The actual string value passed in here here doesn't matter
        // maybe I'll use that for something eventually, or just remove it
        listViewAdapter.add(new EventEntity(null, "", "user", OffsetDateTime.MIN, OffsetDateTime.MIN));
    }

    private void onSaveTaskButtonClicked() {
        final List<EventEntity> eventsToPersist = new ArrayList<>();

        // Check user input and add to list
        // Probably not safe, if the user modifies the entries when saving
        for (int i = 0; i < listViewAdapter.getCount(); i++) {
            final EventEntity eventEntity = (EventEntity) listViewAdapter.getItem(i);

            if (eventEntity != null) {
                if ("".equals(eventEntity.getName())) {
                    Toast.makeText(getActivity(), "Please enter a name for all events",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                if (Duration.ZERO.equals(eventEntity.getDuration())) {
                    Toast.makeText(getActivity(), "Please enter a duration for all events",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                if (OffsetDateTime.MIN.equals(eventEntity.getStartDateTime()) || OffsetDateTime.MIN.equals(eventEntity.getEndDateTime())) {
                    Toast.makeText(getActivity(), "Please complete start/end dates and times for all events",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                eventsToPersist.add(eventEntity);
            }
        }

        for (EventEntity entity : eventsToPersist) {
            MainApplication.getDatabase().eventDao().create(entity);
        }

        Toast.makeText(getActivity(), "Events saved.",
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final List<EventEntity> eventEntities = new ArrayList<>();
        listViewAdapter = new AddEventListAdapter(getActivity(), eventEntities);

        listView = view.findViewById(R.id.list_events);
        listView.setAdapter(listViewAdapter);

        layoutAddEvent = view.findViewById(R.id.layout_add_event);
        layoutAddEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onAddEventButtonClicked();
            }
        });

        saveTaskButton = view.findViewById(R.id.btn_save_task);
        saveTaskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSaveTaskButtonClicked();
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final FloatingActionButton fab = getFloatingActionButton();
        if (fab != null) {
            fab.setVisibility(View.GONE);
        }
    }
}
