package ca.uwaterloo.dtqzzy.ece452.views;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import ca.uwaterloo.dtqzzy.ece452.WorkActivity;
import ca.uwaterloo.dtqzzy.ece452.NavigationOption;

public abstract class WorkFragment extends Fragment {

    @Nullable
    public ActionBar getSupportActionBar() {
        return getActivity() != null ? ((WorkActivity) getActivity()).getSupportActionBar() : null;
    }

    public void setSupportActionBar(@Nullable Toolbar toolbar) {
        if (getActivity() != null) {
            ((WorkActivity) getActivity()).setSupportActionBar(toolbar);
        }
    }

    @Nullable
    public FloatingActionButton getFloatingActionButton() {
        return getActivity() != null ? ((WorkActivity) getActivity()).getFloatingActionButton() : null;
    }

    public void navigate(@Nullable WorkFragment fragment, @NonNull NavigationOption option) {
        if (getActivity() != null) {
            ((WorkActivity) getActivity()).navigate(fragment, option);
        }
    }
}
