package ca.uwaterloo.dtqzzy.ece452.helpers;

import org.threeten.bp.Duration;

public class DurationHelper {
    public static String getFormattedDuration(Duration duration) {
        final StringBuilder builder = new StringBuilder();

        if (duration.toHours() > 0) {
            builder.append(duration.toHours());
            builder.append(" hours ");
        }

        if (duration.toMinutes() > 0) {
            builder.append(duration.toMinutes() - duration.toHours() * 60);
            builder.append(" minutes ");
        }

        if (duration.toMillis() < 60000) {
            builder.append("under a minute");
        }

        return builder.toString().trim();
    }
}
