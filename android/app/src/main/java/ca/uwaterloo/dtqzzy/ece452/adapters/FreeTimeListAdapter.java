package ca.uwaterloo.dtqzzy.ece452.adapters;


import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.threeten.bp.format.DateTimeFormatter;

import java.util.List;

import ca.uwaterloo.dtqzzy.ece452.R;
import ca.uwaterloo.dtqzzy.ece452.models.ScheduledSubtask;
import ca.uwaterloo.dtqzzy.ece452.models.scheduler.FreeTime;

public class FreeTimeListAdapter extends ArrayAdapter<FreeTime> {

    public FreeTimeListAdapter(Activity context, List<FreeTime> freeTimes) {
        super(context, 0, freeTimes);
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_calendar_subtask, parent, false);
        }

        final FreeTime freeTime = getItem(position);
        if (freeTime == null) {
            return convertView;
        }

        final TextView subtaskName = convertView.findViewById(R.id.tv_subtask_name);
        final TextView subtaskTime = convertView.findViewById(R.id.tv_subtask_time);

        subtaskName.setText("Free Time");
        subtaskTime.setText(freeTime.getStartDateTime().format(DateTimeFormatter.ofPattern("MM/dd hh:mm a")) + " to " + freeTime.getEndDateTime().format(DateTimeFormatter.ofPattern("MM/dd hh:mm a")));

        return convertView;

    }
}
