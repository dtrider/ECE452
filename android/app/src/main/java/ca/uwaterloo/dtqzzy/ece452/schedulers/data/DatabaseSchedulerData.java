package ca.uwaterloo.dtqzzy.ece452.schedulers.data;

import java.util.List;

import ca.uwaterloo.dtqzzy.ece452.models.db.EventEntity;
import ca.uwaterloo.dtqzzy.ece452.models.db.RestrictionPeriodEntity;
import ca.uwaterloo.dtqzzy.ece452.models.db.ScheduledSubtaskEntity;
import ca.uwaterloo.dtqzzy.ece452.models.db.SubtaskEntity;
import ca.uwaterloo.dtqzzy.ece452.models.db.WorkDatabase;

public class DatabaseSchedulerData implements SchedulerData {
    private final WorkDatabase database;

    public DatabaseSchedulerData(WorkDatabase database) {
        this.database = database;
    }

    @Override
    public List<RestrictionPeriodEntity> getRestrictions() {
        return database.restrictionPeriodDao().getRestrictions();
    }

    @Override
    public List<EventEntity> getEvents() {
        return database.eventDao().getEvents();
    }

    @Override
    public List<SubtaskEntity> getSubtasks() {
        return database.subtaskDao().getSubtasks();
    }

    @Override
    public List<ScheduledSubtaskEntity> getScheduledSubtasks() {
        return database.scheduledSubtaskDao().getScheduledSubtasks();
    }
}
