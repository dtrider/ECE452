package ca.uwaterloo.dtqzzy.ece452;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import ca.uwaterloo.dtqzzy.ece452.views.AddEventFragment;
import ca.uwaterloo.dtqzzy.ece452.views.AvailabilityFragment;
import ca.uwaterloo.dtqzzy.ece452.views.CalendarFragment;
import ca.uwaterloo.dtqzzy.ece452.views.OverviewFragment;

public class MainActivity extends WorkActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);

        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        final NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_overview:
                        navigate(new OverviewFragment(), NavigationOption.REPLACE_CURRENT);
                        break;

                    case R.id.nav_calendar:
                        navigate(new CalendarFragment(), NavigationOption.REPLACE_CURRENT);
                        break;

                    case R.id.nav_availability:
                        navigate(new AvailabilityFragment(), NavigationOption.REPLACE_CURRENT);
                        break;

                    case R.id.nav_events:
                        navigate(new AddEventFragment(), NavigationOption.REPLACE_CURRENT);
                        break;

                    case R.id.nav_share:
                        Intent intent = new Intent(MainActivity.this, ShareActivity.class);
                        startActivity(intent);

                        break;
                }

                drawerLayout.closeDrawer(GravityCompat.START);
                return false;
            }
        });

        navigate(new OverviewFragment(), NavigationOption.REPLACE_CURRENT);
    }

    @NonNull
    @Override
    public FloatingActionButton getFloatingActionButton() {
        return findViewById(R.id.fab);
    }

    @Override
    public void onBackPressed() {
        final DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        final int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
