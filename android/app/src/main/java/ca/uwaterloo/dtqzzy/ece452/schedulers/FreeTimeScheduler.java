package ca.uwaterloo.dtqzzy.ece452.schedulers;

import android.support.annotation.NonNull;
import android.util.Log;

import org.threeten.bp.OffsetDateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import ca.uwaterloo.dtqzzy.ece452.models.scheduler.FreeTime;

public class FreeTimeScheduler implements Scheduler {
    @Override
    public SchedulerResult schedule() {
        return null;
    }

    private List<List<OffsetDateTime[]>> busyLists;

    public FreeTimeScheduler(List<List<OffsetDateTime[]>> busyLists) {
        this.busyLists = busyLists;
    }

    public List<FreeTime> findFreeTimes() {
        //merge to 1 list
        LinkedList<OffsetDateTime[]> intervalList = new LinkedList<>();
        for (List<OffsetDateTime[]> lists : busyLists) {
            for (OffsetDateTime[] interval : lists) {
                intervalList.add(interval);
            }
        }

        //sort it
        Collections.sort(intervalList, new Comparator<OffsetDateTime[]>() {
            @Override
            public int compare(OffsetDateTime[] sub1, OffsetDateTime[] sub2) {
                return sub1[0].compareTo(sub2[0]);
            }

        });

        LinkedList<OffsetDateTime[]> merged = new LinkedList<>();
        for (OffsetDateTime[] interval : intervalList) {
            if (merged.isEmpty() || merged.getLast()[1].compareTo(interval[0]) < 0) {
                merged.add(interval);
            } else {
                OffsetDateTime max;
                if (merged.getLast()[1].compareTo(interval[1]) > 0) {
                    max = merged.getLast()[1];
                } else {
                    max = interval[1];
                }
                merged.getLast()[1] = max;
            }
        }

        //find common free time
        //get a list of free times
        final List<FreeTime> freeTimes = new ArrayList<>();
        for (int i = 1; i < merged.size(); i++) {
            //opposite to busy time
            final OffsetDateTime start = merged.get(i - 1)[1];
            final OffsetDateTime end = merged.get(i)[0];

            if (start.isAfter(OffsetDateTime.now())) {
                freeTimes.add(new FreeTime(start, end));
            }
        }


        if (freeTimes.size() <= 0) {
            //todo: Display in UI
            Log.e("no common free time", " found");
            return freeTimes;
        }


        for (FreeTime freeTime : freeTimes) {
            if (freeTime.getStartDateTime().compareTo(freeTime.getEndDateTime()) > 60) {
                Log.e(freeTime.toString(), freeTime.getStartDateTime().toString());
                return freeTimes;
            }
        }

        Log.e("no common free time", " longer than 60 mins");
        return freeTimes;

        //sort the list
        /*Collections.sort(mergedList, new Comparator<OffsetDateTime[]>() {
            @Override
            public int compare(OffsetDateTime[] a, OffsetDateTime[] b) {
                return a[0].compareTo(b[0]);
            }
        });*/

        //find common time
        /*Log.e("wa", "wa");
        if (intervalList == null) {
            throw new NullPointerException("Input list cannot be null.");
        }

        final HashSet<OffsetDateTime[]> hashSet = new HashSet<OffsetDateTime[]>();

        for(OffsetDateTime pair[] : intervalList){
            Log.e(pair[0].toString(), pair[1].toString());
        }

        for (int i = 0; i < intervalList.size() - 1; i++) {
            final OffsetDateTime[] intervali = intervalList.get(i);

            for (int j = 0; j < intervalList.size(); j++) {
                final OffsetDateTime[] intervalj = intervalList.get(j);

                if (intervalj[0].compareTo(intervali[1]) < 0 && intervalj[1].compareTo(intervali[0]) > 0 && i != j) {
                    OffsetDateTime start, end;
                    if (intervali[0].compareTo(intervalj[0]) > 0) {
                        start = intervali[0];
                    } else {
                        start = intervalj[0];
                    }

                    if (intervali[1].compareTo(intervalj[1]) < 0) {
                        end = intervali[1];
                    } else {
                        end = intervalj[1];
                    }

                    hashSet.add(new OffsetDateTime[]{start, end});
                }
            }
        }*/
    }

    @NonNull
    @Override
    public SchedulerResult getCachedSchedule() {
        return SchedulerResult.fail();
    }

}
