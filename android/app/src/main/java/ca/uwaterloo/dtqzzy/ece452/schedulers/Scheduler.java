package ca.uwaterloo.dtqzzy.ece452.schedulers;


import android.support.annotation.NonNull;

import java.util.List;

import ca.uwaterloo.dtqzzy.ece452.models.Event;
import ca.uwaterloo.dtqzzy.ece452.models.RestrictionPeriod;
import ca.uwaterloo.dtqzzy.ece452.models.ScheduledSubtask;
import ca.uwaterloo.dtqzzy.ece452.models.Subtask;

public interface Scheduler {
    /**
     * Schedules the subtasks. Each call result is cached in the getCachedSchedule() method.
     */
    SchedulerResult schedule();

    @NonNull
    SchedulerResult getCachedSchedule();
}
