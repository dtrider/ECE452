package ca.uwaterloo.dtqzzy.ece452.views.external;


import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.TimePicker;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalTime;

import java.security.acl.LastOwnerException;

public class TimePickerDialogFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
    private OnTimeSetListener timeSetListener = null;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new TimePickerDialog(getActivity(), this, 0, 0, false);
    }

    @Override
    public void onTimeSet(TimePicker view, int hour, int min) {
        if (timeSetListener != null) {
            timeSetListener.onTimeSet(view, LocalTime.of(hour, min));
        }
    }

    public void setOnTimeSetListener(OnTimeSetListener onTimeSetListener) {
        timeSetListener= onTimeSetListener;
    }

    public static abstract class OnTimeSetListener {
        public abstract void onTimeSet(TimePicker view, LocalTime time);
    }
}
