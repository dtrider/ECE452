package ca.uwaterloo.dtqzzy.ece452.schedulers;

import android.support.annotation.NonNull;

import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.temporal.ChronoUnit;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;

import ca.uwaterloo.dtqzzy.ece452.MainApplication;
import ca.uwaterloo.dtqzzy.ece452.models.ScheduledSubtask;
import ca.uwaterloo.dtqzzy.ece452.models.Subtask;
import ca.uwaterloo.dtqzzy.ece452.models.db.ScheduledSubtaskEntity;

public class SubtaskScheduler implements Scheduler {
    @NonNull private List<Subtask> subtasks;
    @NonNull private List<ScheduledSubtask> scheduledSubtasks = new ArrayList<>();
    private Map<OffsetDateTime, Long> freeTimes;

    public SubtaskScheduler(List<Subtask> subtasks, Map<OffsetDateTime, Long> freeTimes) {
        this.subtasks = subtasks;
        this.freeTimes = freeTimes;
    }

    public class Results {
        List<ScheduledSubtask> scheduled;
        List<Subtask> unscheduled;

        public Results(List<ScheduledSubtask> scheduled, List<Subtask> unscheduled) {
            this.scheduled = scheduled;
            this.unscheduled = unscheduled;
        }
    }

    public SchedulerResult schedule() {
        Results result = scheduleHelper();
        if (result.unscheduled.size() > 0) {
            return SchedulerResult.conflict(result.scheduled, result.unscheduled);
        } else {
            return SchedulerResult.ok(result.scheduled);
        }
    }

    private Results scheduleHelper() {
        List<Subtask> unscheduled = new ArrayList<>();
        ListIterator<Subtask> sb = subtasks.listIterator();

        for (Subtask subtask : subtasks) {
            for (Iterator<Map.Entry<OffsetDateTime, Long>> freeTime = freeTimes.entrySet().iterator(); freeTime.hasNext(); ) {
                //if (freeTimes.isEmpty()) break;

                final Entry<OffsetDateTime, Long> currentFreeTime = freeTime.next();
                final Long taskDuration = subtask.getDuration().toMinutes();
                final OffsetDateTime taskDeadline = subtask.getDeadline();
                final Long freeDuration = currentFreeTime.getValue();

                //if cannot schedule before deadline, no need to keep iterating
                if (taskDeadline.compareTo(currentFreeTime.getKey()) < 0) {
                    unscheduled.add(subtask);
                    break;
                }

                if (taskDuration < freeDuration) {
                    final OffsetDateTime endTime = currentFreeTime.getKey().plus(taskDuration, ChronoUnit.MINUTES);
                    ScheduledSubtaskEntity stEntity = new ScheduledSubtaskEntity(
                            null, subtask, currentFreeTime.getKey(), endTime);
                    scheduledSubtasks.add(stEntity);
                    MainApplication.getDatabase().scheduledSubtaskDao().create(stEntity);

                    freeTime.remove();
                    //freeTimes.put(endTime.plus(20, ChronoUnit.MINUTES),
                    //       freeDuration - taskDuration);
                    break;
                }
            }
        }
        return new Results(scheduledSubtasks, unscheduled);


        //Schedule each subtask

        /*while (sb.hasNext()) {
            for (Iterator<Map.Entry<OffsetDateTime, Long>> freeTime = freeTimes.entrySet().iterator(); freeTime.hasNext(); ) {
                final Subtask currentSub = sb.next();
                final Entry<OffsetDateTime, Long> currentFreeTime = freeTime.next();
                final Long taskDuration = currentSub.getDuration().toMinutes();
                final Long freeDuration = currentFreeTime.getValue();

                if(currentSub.getDeadline().compareTo(currentFreeTime.getKey()) >=0) continue;

                if (taskDuration < freeDuration) {
                    final OffsetDateTime endTime = currentFreeTime.getKey().plus(taskDuration, ChronoUnit.MINUTES);

                    scheduledSubtasks.add(new ScheduledSubtaskEntity(
                            null, currentSub, currentFreeTime.getKey(), endTime));
                    freeTime.remove();
                    //freeTimes.put(endTime.plus(20, ChronoUnit.MINUTES),
                    //       freeDuration - taskDuration);
                    break;
                }else{
                    unscheduled.add(currentSub);
                }
            }

            //a subtask is not scheduled if code reaches here


        }*/


        //return scheduledSubtasks;
    }

    @NonNull
    public SchedulerResult getCachedSchedule() {
        return SchedulerResult.fail();
    }

    protected void setCachedSchedule(@NonNull List<ScheduledSubtask> scheduledSubtasks) {
        this.scheduledSubtasks = scheduledSubtasks;
    }

}
