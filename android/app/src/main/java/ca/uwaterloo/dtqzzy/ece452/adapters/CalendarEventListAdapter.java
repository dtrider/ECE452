package ca.uwaterloo.dtqzzy.ece452.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.threeten.bp.format.DateTimeFormatter;

import java.util.List;

import ca.uwaterloo.dtqzzy.ece452.R;
import ca.uwaterloo.dtqzzy.ece452.models.Event;

public class CalendarEventListAdapter<T extends Event> extends ArrayAdapter<T> {

    public CalendarEventListAdapter(Activity context, List<T> events) {
        super(context, 0, events);
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_calendar_event, parent, false);
        }

        final Event event = getItem(position);
        if (event == null) {
            return convertView;
        }

        final TextView eventName = convertView.findViewById(R.id.tv_event_name);
        final TextView eventTime = convertView.findViewById(R.id.tv_event_time);

        eventName.setText(event.getName());
        eventTime.setText(event.getStartDateTime().format(DateTimeFormatter.ofPattern("MM/dd hh:mm a")) + " to " + event.getEndDateTime().format(DateTimeFormatter.ofPattern("MM/dd hh:mm a")));

        return convertView;

    }
}
