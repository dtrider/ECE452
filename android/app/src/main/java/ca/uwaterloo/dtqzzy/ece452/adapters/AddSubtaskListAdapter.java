package ca.uwaterloo.dtqzzy.ece452.adapters;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.threeten.bp.Duration;
import org.threeten.bp.LocalDate;
import org.threeten.bp.OffsetTime;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.List;

import ca.uwaterloo.dtqzzy.ece452.R;
import ca.uwaterloo.dtqzzy.ece452.helpers.DurationHelper;
import ca.uwaterloo.dtqzzy.ece452.models.Subtask;
import ca.uwaterloo.dtqzzy.ece452.views.external.DatePickerDialogFragment;
import ca.uwaterloo.dtqzzy.ece452.views.external.DurationPickerDialogFragment;
import mobi.upod.timedurationpicker.TimeDurationPicker;


public class AddSubtaskListAdapter<T extends Subtask> extends ArrayAdapter<T> {
    public AddSubtaskListAdapter(Activity context, List<T> subtasks) {
        super(context, 0, subtasks);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_add_subtask, parent, false);
        }

        final Subtask subtask = getItem(position);
        if (subtask == null) {
            return convertView;
        }

        final EditText subtaskName = convertView.findViewById(R.id.et_subtask_name);
        final TextView subtaskTime = convertView.findViewById(R.id.tv_estimated_time);
        final TextView subtaskDeadline = convertView.findViewById(R.id.tv_deadline_input);
        final ImageView deleteSubtask = convertView.findViewById(R.id.iv_delete_subtask);

        subtaskName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                subtask.setName(editable.toString());
            }
        });

        subtaskTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final DurationPickerDialogFragment durationPicker = new DurationPickerDialogFragment();
                durationPicker.setOnCompleteAction(new DurationPickerDialogFragment.OnDurationSetListener() {
                    @Override
                    public void onDurationSet(TimeDurationPicker view, Duration duration) {
                        subtask.setDuration(duration);
                        subtaskTime.setText(getContext().getString(
                                R.string.subtask_duration, DurationHelper.getFormattedDuration(subtask.getDuration())));
                    }
                });
                durationPicker.show(((FragmentActivity) getContext()).getFragmentManager(), "duration");
            }
        });

        subtaskDeadline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final DatePickerDialogFragment datePicker = new DatePickerDialogFragment();
                datePicker.setOnDateSetListener(new DatePickerDialogFragment.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, LocalDate date) {
                        subtask.setDeadline(date.atTime(OffsetTime.of(0, 0, 0, 0, ZoneOffset.UTC)));
                        subtaskDeadline.setText(getContext().getString(
                                R.string.subtask_deadline,
                                subtask.getDeadline().format(DateTimeFormatter.ofPattern("MM/dd"))));
                    }
                });
                datePicker.show(((FragmentActivity) getContext()).getFragmentManager(), "date");
            }
        });

        deleteSubtask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                remove((T) subtask);
            }
        });

        return convertView;

    }
}
