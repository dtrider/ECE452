package ca.uwaterloo.dtqzzy.ece452.schedulers;


import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ca.uwaterloo.dtqzzy.ece452.models.ScheduledSubtask;
import ca.uwaterloo.dtqzzy.ece452.models.Subtask;

public class SchedulerResult {
    private final SchedulerStatus status;
    private final List<ScheduledSubtask> scheduledSubtasks;
    private final List<Subtask> conflictSubtasks;

    private SchedulerResult(@NonNull SchedulerStatus status, @NonNull List<ScheduledSubtask> scheduledSubtasks, @NonNull List<Subtask> conflictSubtasks) {
        this.status = status;
        this.scheduledSubtasks = scheduledSubtasks;
        this.conflictSubtasks = conflictSubtasks;
    }

    public static SchedulerResult ok(@NonNull List<ScheduledSubtask> scheduledSubtasks) {
        return new SchedulerResult(
                SchedulerStatus.OK,
                Collections.unmodifiableList(scheduledSubtasks),
                Collections.<Subtask>emptyList());
    }

    public static SchedulerResult conflict(@NonNull List<ScheduledSubtask> scheduledSubtasks, @NonNull List<Subtask> conflictSubtasks) {
        return new SchedulerResult(
                SchedulerStatus.CONFLICT,
                Collections.unmodifiableList(scheduledSubtasks),
                Collections.unmodifiableList(conflictSubtasks));
    }

    public static SchedulerResult fail() {
        return new SchedulerResult(
                SchedulerStatus.FAIL,
                Collections.<ScheduledSubtask>emptyList(),
                Collections.<Subtask>emptyList()
        );
    }

    public SchedulerStatus getStatus() {
        return status;
    }

    public List<ScheduledSubtask> getScheduledSubtasks() {
        return scheduledSubtasks;
    }

    public List<Subtask> getConflictSubtasks() {
        return conflictSubtasks;
    }

    enum SchedulerStatus {
        OK, CONFLICT, FAIL
    }
}
