package ca.uwaterloo.dtqzzy.ece452.notification;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.temporal.ChronoUnit;

import java.util.List;

import ca.uwaterloo.dtqzzy.ece452.MainActivity;
import ca.uwaterloo.dtqzzy.ece452.R;
import ca.uwaterloo.dtqzzy.ece452.models.ScheduledSubtask;

public class NotificationHandler extends IntentService {

    public NotificationHandler() {
        super("my notification handler");
    }

    /*public void ScheduleNotification(Context context, List<ScheduledSubtask> tasks) {
        int notificationId = 1;
        for (ScheduledSubtask task : tasks) {
            scheduleNotification(context, task, notificationId);
            notificationId++;
        }
    }*/


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

    }

    public void scheduleNotification(Context context, List<ScheduledSubtask> tasks) {
        int notificationId = 1;
        long delay = 0;

        for (ScheduledSubtask task : tasks) {
            //figure out delay

            OffsetDateTime current = OffsetDateTime.of(2018, OffsetDateTime.now().getMonthValue(), OffsetDateTime.now().getDayOfMonth(),
                    OffsetDateTime.now().getHour(), OffsetDateTime.now().getMinute(), OffsetDateTime.now().getSecond(), 0, org.threeten.bp.ZoneOffset.UTC);
            delay = current.until(task.getStartDateTime(), ChronoUnit.SECONDS);
            Log.e("task", task.getStartDateTime() + "");
            Log.e("delay", delay + "");

            scheduleEachNotification(context, task, notificationId, Math.abs(delay) * 1000);
            notificationId++;
        }
    }

    private void scheduleEachNotification(Context context, ScheduledSubtask task, int notificationId, long delay) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            final NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                final String channelId = "uwork_channel";
                final CharSequence channelName = "UWork Channel";
                final int importance = NotificationManager.IMPORTANCE_HIGH;

                if (notificationManager.getNotificationChannel(channelId) == null) {
                    final NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, importance);
                    notificationChannel.enableLights(true);
                    notificationChannel.setLightColor(Color.RED);
                    notificationChannel.enableVibration(true);
                    notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

                    notificationManager.createNotificationChannel(notificationChannel);
                }
            }
        }

        //delay is after how much time(in millis) from current time you want to schedule the notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "uwork_channel")
                .setContentTitle("Scheduled Task")
                .setContentText("It is time to do " + task.getSubtask().getName())
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent activity = PendingIntent.getActivity(context, notificationId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        builder.setContentIntent(activity);

        final Notification notification = builder.build();

        Intent notificationIntent = new Intent(context, NotificationPub.class);
        notificationIntent.putExtra(NotificationPub.NOTIFICATION_ID, notificationId);
        notificationIntent.putExtra(NotificationPub.NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, notificationId, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);


        final long futureInMillis = SystemClock.elapsedRealtime() + delay;
        Log.e("future milli seconds", futureInMillis + "");

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
    }
}
